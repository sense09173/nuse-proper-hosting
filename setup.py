import setuptools

with open("README.md", "r", encoding="UTF-8") as f:
    long_description = f.read()

setuptools.setup(
    name="nuse-proper-hosting",
    version="1.0.0",
    author="seeun0917",
    author_email="seeun0917@naver.com",
    description="Feel free to use replit hosting.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/sense09173/nuse-proper-hosting",
    packages=setuptools.find_packages()
)
