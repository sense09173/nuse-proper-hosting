# nuse-proper-hosting
Feel free to use replit hosting :D

## Installation
```
pip install -U nuse-proper-hosting
```

## Example

```py
from nuse_proper_hosting import detect_repl

detect_repl()
```
## Unblock Replit

```
1. Find import code with Ctrl + F3, and add 'n'
like this:

from use_proper_hosting import detect_repl
➜
from nuse_proper_hosting import detect_repl

2. Feel free to use replit hosting XD
```
